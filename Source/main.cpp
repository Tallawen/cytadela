#include "StdAfx.h"

#include "Screen/Screen.h"

#include "Config.h"

Logger::Log logger;

int main() {
    Global global;

    /* Załączanie loggera/loggerów */
    Logger::TextOutput   *logTextOutput    = new Logger::TextOutput("Logger/log.txt");
    Logger::HtmlOutput   *logHtmlOutput    = new Logger::HtmlOutput("Logger/log.html");
    Logger::ConsolOutput *logConsolOutput  = new Logger::ConsolOutput();

    logConsolOutput->setPriorityFilter(Logger::LP_Debug);

    logger.addOutput(logTextOutput);
    logger.addOutput(logHtmlOutput);
    logger.addOutput(logConsolOutput);

    sf::ContextSettings settings;
    settings.antialiasingLevel = 16;

    Renderer::Window window(sf::VideoMode(800, 600), "Window", sf::Style::Default, settings);

    Renderer::FPSCounter fps;
    fps.setColor(sf::Color::Red);

    Screen::Screen screen;

    sf::Clock deltaTimer;
    float delta;

    screen.change(new Screen::Authors);

     sf::Event event;
    while(window.isOpen()) {
        while(window.pollEvent(event)) {
            screen.process(event);

            if(event.type == sf::Event::Closed)
                window.close();

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::N))
                screen.change(new Screen::Intro);

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::P))
                screen.change(new Screen::Authors);
        }

        delta = deltaTimer.restart().asSeconds();

        screen.update(delta);

        window.clear();

        screen.draw(&window);

        window.draw(fps);
        window.display();
    }
  return 0;
}
