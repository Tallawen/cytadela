#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include "Utils/Pattern/Singleton.h"

class Global : public Utils::Pattern::Singleton<Global> {
public:
    /************************************************/
    Global();

    /************************************************/
    ~Global();
};

#endif /*__GLOBAL_H__*/
