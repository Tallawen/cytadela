#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "Utils/Logger.h"

/************************************************
 * Wersja
 ************************************************/
#define VERSION_MAJOR 0
#define VERSION_MINOR 1

/************************************************
 * Rozpoznawanie systemu
 ************************************************/
#if defined(_WIN32) || defined(__WIN32__)
// Windows
#define SYSTEM_WINDOWS

#elif defined(linux) || defined(__linux) // Linux
#define SYSTEM_LINUX

#else // Inny
#error This operating system is not supported

#endif

/************************************************
 * Makro debugowania
 ************************************************/
#if !defined(DEBUG)
#define DEBUG
#endif

/************************************************
 * Makro assert
 ************************************************/
static void Noop() {
}

static void TerminateProgram() {
#if (defined(__MINGW32__) || defined(__GNUC__))
    abort(); // trap; generates core dump
#else
    exit(1); // goodbye cruel world
#endif
}

#if !defined(ASSERT)
    #if !defined(DEBUG)
        #define ASSERT(cond, what) hgNoop()
    #else
        #define ASSERT(cond, what) if(!cond) { \
                                       LoggerFatal << what << LoggerFlush; \
                                       TerminateProgram(); \
                                   } else { \
                                       Noop(); \
                                   }
    #endif
#endif

#endif /*__CONFIG_H__*/
