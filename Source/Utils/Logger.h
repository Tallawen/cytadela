#ifndef __UTILS_LOGGER_H__
#define __UTILS_LOGGER_H__

#include <Logger/Logger.h>

extern Logger::Log logger;

/********************************************//**
 @brief Makro zmieniajace priorytet loggera
 ************************************************/
#ifndef LOGGER_PRIORITY
    #define LOGGER_PRIORITY(priority) LOG_INFO(logger) << Logger::Prio(priority)
#endif


/********************************************//**
 * @brief Makro odwołujące się do loggowania informacji
 ************************************************/
#ifndef LoggerInfo
    #define LoggerInfo LOGGER_PRIORITY(Logger::LP_Info)
#endif


/********************************************//**
 * @brief Makro odwołujące się do loggowania komunikatów debugowania
 ************************************************/
#ifndef LoggerDebug
    #define LoggerDebug LOGGER_PRIORITY(Logger::LP_Debug)
#endif


/********************************************//**
 * @brief Makro odwołujące się do loggowania ostrzenia
 ************************************************/
#ifndef LoggerWarning
    #define LoggerWarning LOGGER_PRIORITY(Logger::LP_Warning)
#endif


/********************************************//**
 * @brief Makro odwołujące się do loggowania sukcesu
 ************************************************/
#ifndef LoggerSuccess
    #define LoggerSuccess LOGGER_PRIORITY(Logger::LP_Success)
#endif


/********************************************//**
 * @brief Makro odwołujące się do loggowania błedu
 ************************************************/
#ifndef LoggerError
    #define LoggerError LOGGER_PRIORITY(Logger::LP_Error)
#endif


/********************************************//**
 * @brief Makro odwołujące się do loggowaniafatalnego błedu
 ************************************************/
#ifndef LoggerFatal
    #define LoggerFatal LOGGER_PRIORITY(Logger::LP_Fatal)
#endif


/********************************************//**
 * @brief Makro czyszczące buffor
 ************************************************/
#ifndef LoggerFlush
    #define LoggerFlush Logger::Flush()
#endif

#endif /*__UTILS_LOGGER_H__*/
