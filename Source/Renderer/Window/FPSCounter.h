#ifndef __RENDERER_WINDOW_FPSCOUNTER_H__
#define __RENDERER_WINDOW_FPSCOUNTER_H__

#include "../../StdAfx.h"

#include <chrono>

namespace Renderer {

    /**********************************************//**
     * @brief Klasa liczy ile jest wyswietlanych klatek na sekunde. Oraz wyswietla ich ilosc.
     *
     * Do dzialania wymagane jest jednokrotne
     * wyrysowanie obiektu co klatke.
     *************************************************/
    class FPSCounter : public sf::Transformable, public sf::Drawable {
    private:
        mutable std::chrono::time_point<std::chrono::high_resolution_clock> currTime;
        mutable std::chrono::time_point<std::chrono::high_resolution_clock> prevTime;
        mutable unsigned int framesCount;
        mutable double timeLimit;
        mutable double _fps;

        bool enabled;

        sf::Font font;
        mutable sf::Text fps_text;

    public:
        /**********************************************//**
         * @brief Konstruktor
         *************************************************/
        FPSCounter(const sf::Vector2f &_screen_position = sf::Vector2f(0.f, 0.f), const sf::Color &_color = sf::Color::Yellow, unsigned int _size = 20, const std::string &_font_name = "arial.ttf");

        /**********************************************//**
         * @brief Destruktor
         *************************************************/
        ~FPSCounter();

    protected:
        /**********************************************//**
         * @copybrief darw
         *************************************************/
        void draw(sf::RenderTarget &_target, sf::RenderStates _states) const override;

        /**********************************************//**
         * @copybrief Konwertuje wartośc FPS na string
         *************************************************/
        void fpsToString() const;

        /**********************************************//**
         * @copybrief Metoda wykonywująca się przy każdym obejściu pętli
         *************************************************/
        void step() const;

    public:
        /**********************************************//**
         * @copybrief Zwraca wartosc liczbowa klatek na sekunde
         *************************************************/
        double fps();

        /**********************************************//**
         * @copybrief Ustawianie koloru tekstu
         *************************************************/
        void setColor(const sf::Color &_color);

        /**********************************************//**
         * @copybrief Ustawienie wielkosci czcionki
         *************************************************/
        void setFontSize(unsigned int _size);

        /**********************************************//**
         * @copybrief Wlaczenie/wylaczenie wyswietlania
         *************************************************/
        void enable(bool _on = true);
    };

}

#endif // __RENDERER_WINDOW_FPSCOUNTER_H__
