#ifndef __RENDERER_WINDOW_WINDOW_H__ /* Zmieniłem ponieważ przy większej ilości plików z takim define kod zostanie pominięty */
#define __RENDERER_WINDOW_WINDOW_H__

#include "..\..\StdAfx.h"

namespace Renderer {

    /**********************************************//**
     * @brief Klasa odpowiedziala za glowne okno
     *************************************************/
    class Window : public sf::RenderWindow {
    private:
        sf::VideoMode        mode;
        sf::String           title;
        sf::Uint32           style;
        sf::ContextSettings  settings;
        sf::Texture          _screenShot;

    public:
        /**********************************************//**
         * @brief Konstruktor domyślny
         *************************************************/
        Window();

        /**********************************************//**
         * @brief Konstruktor
         *
         * @param _mode     : Tryb okna
         * @param _title    : Tytuł okna
         * @param _style    : Styl okna
         * @param _settings : Dodatkowe ustawienia
         *************************************************/
        Window(sf::VideoMode _mode, const sf::String &_title, sf::Uint32 _style = sf::Style::Default, const sf::ContextSettings &_settings = sf::ContextSettings());

        //explicit Window(sf::WindowHandle handle, const sf::ContextSettings& settings = sf::ContextSettings::ContextSettings());

    public:
        /**********************************************//**
         * @brief Metoda zmienia rozmiar okna
         *
         * @param _size : Nowy rozmiar
         *************************************************/
        void resize(const sf::Vector2u &_size);

        /**********************************************//**
         * @brief Metoda zmienia styl okna
         *
         * @param _style : Nowy styl
         *************************************************/
        void setStyle(sf::Uint32 _style);

        /**********************************************//**
         * @brief Metoda tworzy screenshot okna
         *************************************************/
        sf::Sprite screenshot();

    protected:
        /**********************************************//**
         * @brief Obsługa eventu zmiany rozmiaru okna
         *************************************************/
        void onResize() override;

    };

}

#endif // __RENDERER_WINDOW_WINDOW_H__
